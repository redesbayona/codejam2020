package nestingdepth;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SolutionLocalTester extends Solution {


    public static void main(String[] args) {

        try {
            if (args.length > 0) {
                in = new Scanner(new File(args[0]));
            } else {
                in = new Scanner(new File("test.txt"));
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }

        Solution.main(args);
    }


}
